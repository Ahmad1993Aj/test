import React, { useState } from 'react';

export default function App() {
	const questions = [
		{
			questionText: 'Who developed React?',
			answerOptions: [
				{ answerText: 'Jordan Walke', isCorrect: true },
				{ answerText: 'Elon Musk', isCorrect: false },
				{ answerText: 'Bill Gates', isCorrect: false },
				{ answerText: 'Guido van Rossum', isCorrect: false },
			],
		},
		
		{
			questionText: 'When was React released?',
			answerOptions: [
				{ answerText: '2014', isCorrect: false },
				{ answerText: '2013', isCorrect: true },
				{ answerText: '2011', isCorrect: false },
				{ answerText: '2016', isCorrect: false },
			],
		},
		{
			questionText: 'What programming language does React use?',
			answerOptions: [
				{ answerText: 'JavaScript', isCorrect: true },
				{ answerText: 'Python', isCorrect: false },
				{ answerText: 'Angular', isCorrect: false },
				{ answerText: 'PHP', isCorrect: false },
				{ answerText: 'Node.js', isCorrect: false },
				{ answerText: 'C++', isCorrect: false },

			],
		},
		{
			questionText: 'What goes up but never ever comes down?',
			answerOptions: [
				{ answerText: 'Temperature ', isCorrect: false },
				{ answerText: 'Power', isCorrect: false },
				{ answerText: 'Numbers', isCorrect: false },
				{ answerText: 'Your Age', isCorrect: true },
				{ answerText: 'Body weight', isCorrect: false },
			],
		},
	];



	const [showScore, setShowScore] = useState (false);
	const [currentQuestion, setCurrentQuestion] = useState(0);


	const [score, setScore] = useState(0);

	const handlenAnswerButtonClick = (isCorrect) => {
		if(isCorrect===true){
			setScore(score+1);
		}

		const nextQuestion = currentQuestion+1;
		if (nextQuestion < questions.length){
			setCurrentQuestion(nextQuestion);
		}
			else {
				setShowScore(true);
			}
		
		setCurrentQuestion(nextQuestion);

	}


	return (
		<div className='app'>
			{/* HINT: replace "false" with logic to display the 
      score when the user has answered all the questions */}
			{showScore ? (
				<div className='score-section'>You scored {score} out of {questions.length}</div>
			) : (
				<>
					<div className='question-section'>
						<div className='question-count'>
							<span>Question {currentQuestion+1}</span>/{questions.length}
						</div>
						<div className='question-text'>{questions[currentQuestion].questionText}</div>
					</div>
					<div className='answer-section'>
					{
						questions[currentQuestion].answerOptions.map((answerOptions)=>
						<button onClick={() => handlenAnswerButtonClick(answerOptions.isCorrect)}>{answerOptions.answerText}
						</button>)
					}

					</div>
				</>
			)}
		</div>
	);
}
